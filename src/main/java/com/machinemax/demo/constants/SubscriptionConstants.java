package com.machinemax.demo.constants;

import java.util.LinkedList;

public final class SubscriptionConstants {
    public final static LinkedList<String> SubscriptionAlertType= new LinkedList<>();
    public final static LinkedList<String> SubscriptionAlertChannel= new LinkedList<>();
            static{
                SubscriptionAlertType.add("SERVICE_REQUIRED");
                SubscriptionAlertType.add("INSURANCE_PAYMENT");
                SubscriptionAlertType.add("OTA_SOFTWARE_UPDATE");
                SubscriptionAlertType.add("ANNOUNCEMENT_FOR_OFFERS");

                SubscriptionAlertChannel.add("EMAIL");
                SubscriptionAlertChannel.add("SMS");
                SubscriptionAlertChannel.add("WHATSAPP");
            }
}
