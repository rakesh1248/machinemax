package com.machinemax.demo.controller;

import com.machinemax.demo.constants.SubscriptionConstants;
import com.machinemax.demo.dao.Subscription;
import com.machinemax.demo.service.SubscriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class SubscriptionController {
    @Autowired
    Subscription subscription;
    @Autowired
    SubscriptionService service;

    @PostMapping(path = "subscribe", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Subscription> createSubscription(@RequestBody Subscription subscription) {
        if (subscription == null) {
            System.out.println("Object is null");
            return new ResponseEntity<>(null, HttpStatus.NOT_IMPLEMENTED);
        } else if (SubscriptionConstants.SubscriptionAlertChannel.contains(subscription.getSubscriptionChannel()) && SubscriptionConstants.SubscriptionAlertType.contains(subscription.getSubscriptionAlertType())) {
            System.out.println("Created!!");
            return service.subscribe(subscription);
        }
        else {
            System.out.println("Error in processing");
            return new ResponseEntity<>(null, HttpStatus.NOT_IMPLEMENTED);
        }
    }
}
