package com.machinemax.demo.dao;

import org.springframework.stereotype.Component;

@Component
public class Subscription{
    int threshold;
    String interval;

    String email;

    String phone;

    String subscriptionAlertType;

    String subscriptionChannel;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }



    public String getSubscriptionAlertType() {
        return subscriptionAlertType;
    }

    public void setSubscriptionAlertType(String subscriptionAlertType) {
        this.subscriptionAlertType = subscriptionAlertType;
    }

    public String getSubscriptionChannel() {
        return subscriptionChannel;
    }

    public void setSubscriptionChannel(String subscriptionChannel) {
        this.subscriptionChannel = subscriptionChannel;
    }


    public int getThreshold() {
        return threshold;
    }

    public void setThreshold(int threshold) {
        this.threshold = threshold;
    }

    public String getInterval() {
        return interval;
    }

    public void setInterval(String interval) {
        this.interval = interval;
    }

}
