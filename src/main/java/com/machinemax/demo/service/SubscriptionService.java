package com.machinemax.demo.service;

import com.machinemax.demo.dao.Subscription;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.auth.credentials.EnvironmentVariableCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.sns.SnsClient;
import software.amazon.awssdk.services.sns.model.SnsException;
import software.amazon.awssdk.services.sns.model.SubscribeRequest;
import software.amazon.awssdk.services.sns.model.SubscribeResponse;
@Service
public class SubscriptionService {
    @Value("topicArn")
    String topicArn;
    public ResponseEntity subscribe(Subscription subscription)
    {
        SnsClient snsClient = SnsClient.builder()
                .region(Region.US_EAST_1)
                .credentialsProvider(EnvironmentVariableCredentialsProvider.create())
                .build();
        if(subscription.getSubscriptionAlertType().equals("EMAIL"))
        {
            subEmail(snsClient,topicArn,subscription.getEmail());
            return new ResponseEntity<>(subscription, HttpStatus.CREATED);
        } else if (subscription.getSubscriptionAlertType().equals("SMS")) {
            subSMS(snsClient,topicArn,subscription.getEmail());
            return new ResponseEntity<>(subscription, HttpStatus.CREATED);
        }
        else if (subscription.getSubscriptionAlertType().equals("WHATSAPP")) {
            ///WHATSAPP MODULE CALL REQUIRED////
            return new ResponseEntity<>(subscription, HttpStatus.CREATED);
        }
        else {
            return new ResponseEntity<>(null, HttpStatus.NOT_IMPLEMENTED);
        }


    }
    public static void subEmail(SnsClient snsClient, String topicArn, String email) {

        try {
            SubscribeRequest request = SubscribeRequest.builder()
                    .protocol("email")
                    .endpoint(email)
                    .returnSubscriptionArn(true)
                    .topicArn(topicArn)
                    .build();

            SubscribeResponse result = snsClient.subscribe(request);
            System.out.println("Subscription ARN: " + result.subscriptionArn() + "\n\n Status is " + result.sdkHttpResponse().statusCode());

        } catch (SnsException e) {
            System.err.println(e.awsErrorDetails().errorMessage());
            System.exit(1);
        }
    }
        public static void subSMS(SnsClient snsClient, String topicArn, String phone) {

            try {
                SubscribeRequest request = SubscribeRequest.builder()
                        .protocol("sms")
                        .endpoint(phone)
                        .returnSubscriptionArn(true)
                        .topicArn(topicArn)
                        .build();

                SubscribeResponse result = snsClient.subscribe(request);
                System.out.println("Subscription ARN: " + result.subscriptionArn() + "\n\n Status is " + result.sdkHttpResponse().statusCode());

            } catch (SnsException e) {
                System.err.println(e.awsErrorDetails().errorMessage());
                System.exit(1);
            }
    }

}
